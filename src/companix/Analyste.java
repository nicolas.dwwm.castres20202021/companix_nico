package companix;

public class Analyste extends Salarie {
	
	private int nbreDeplacement;

	public Analyste(String nom, String prenom, String dateEmbauche, int code, int nbreDeplacement) {
		super(nom, prenom, dateEmbauche, code);
		this.nbreDeplacement = nbreDeplacement;
	}

	public int getNbreDeplacement() {
		return nbreDeplacement;
	}

	public void setNbreDeplacement(int nbreDeplacement) {
		this.nbreDeplacement = nbreDeplacement;
	}
	
}
