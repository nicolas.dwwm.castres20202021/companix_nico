package companix;

public class Concepteur extends Salarie {
	
	private int nbreAnneDev;

	public Concepteur(String nom, String prenom, String dateEmbauche, int code, int nbreAnneDev) {
		super(nom, prenom, dateEmbauche, code);
		this.nbreAnneDev = nbreAnneDev;
		
	}

	public int getNbreAnneDev() {
		return nbreAnneDev;
	}

	public void setNbreAnneDev(int nbreAnneDev) {
		this.nbreAnneDev = nbreAnneDev;
	}
	
}
