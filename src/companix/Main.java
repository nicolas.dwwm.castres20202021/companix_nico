package companix;
import java.util.*;

public class Main {
	
	static Map<Integer, Salarie> salarie = new HashMap<Integer, Salarie>();
	//Pour que le programme ne s'arrete pas faire "while true"
	//Envoyer un git du livrable par email au prof

	public static void main(String[] args) {
		while(1==1) {
		afficheChoix();
		String str = recupData();
		ChoixTraitement(str);
		}
	}
	
	
	static void afficheChoix() {
        System.out.println("1- Ajouter un concepteur" 
        		+ "\n2- Supprimer un concepteur"
                + "\n3- Lister les concepteurs existants"
        		+ "\n4- Ajouter un analyste" 
                + "\n5- Supprimer un analyste"
                + "\n6- Lister les salariés existants"
        		+ "\n-- Veuillez faire votre choix");
        }
	
	static String recupData() {
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		System.out.println("Vous avez saisi : " + str);
		return str;
	}
	
	static void createConcepteur() {
		System.out.println("Exemple de syntaxe poyur creer un concepteur :" 
				+ "\n-  0002-PESTANA-Cyril-23/12/2014-2"
				+ "\n-  CODE-NOM-PRENOM-DATE-NBRANNES"
				+ "\n-  MERCI DE RENTRER LES DONNES");
		
		String str = recupData();
		String[] arr = str.split("-");
        int code =  Integer.parseInt(arr[0]);
        String nom = arr[1];
        String prenom = arr[2];
        String date = arr[3];
        int annee = Integer.parseInt(arr[4]);
        
        Concepteur monConcepteur = new Concepteur(nom, prenom, date, code, annee);
        
        salarie.put(code, monConcepteur);
		
		}
	
	static void deleteConcepteur() {
		System.out.println("Supprimer un concepteur :" 
				+ "\n-  Rentrer le code du concepteur à supprimer ");
		
		String str = recupData();
		
		Salarie monSalarie = salarie.get(str);
		
		if (monSalarie instanceof Analyste)
			System.out.println("Non, c'est un analyste, pas un concepteur !");
		
		if (monSalarie instanceof Concepteur)
			salarie.remove(str);
	}
	
	static void ChoixTraitement(String str) {

		if (str.equals("1"))
		createConcepteur();
		
		if (str.equals("2"))
		deleteConcepteur();
		
		if (str.equals("3"))
		listeConcepteur();
		
		if (str.equals("4"))
		createAnalyste();
		
		if (str.equals("5"))
		deleteAnalyste();
		
		if (str.equals("6"))
		listeSalarie(salarie);
		
//		if (str.equals("7"))
//		end();
		
	}



	private static void deleteAnalyste() {
		System.out.println("Supprimer un analyste :" 
				+ "\n-  Rentrer le code de l'anaaddAnalystelyste à supprimer ");
		
		String str = recupData();
		
		Salarie monAnalyste = salarie.get(str);
		
		if (monAnalyste instanceof Concepteur)
			System.out.println("Non, c'est un analyste, pas un analyste !");
		
		if (monAnalyste instanceof Analyste)
			salarie.remove(str);
		
	}


	private static void createAnalyste() {
		System.out.println("Exemple de syntaxe poyur creer un analyste :" 
				+ "\n-  0002-PESTANA-Cyril-23/12/2014-2"
				+ "\n-  CODE-NOM-PRENOM-DATE-NBRDEP"
				+ "\n-  MERCI DE RENTRER LES DONNES");
		
		String str = recupData();
		String[] arr = str.split("-");
        int code =  Integer.parseInt(arr[0]);
        String nom = arr[1];
        String prenom = arr[2];
        String date = arr[3];
        int annee = Integer.parseInt(arr[4]);
        
        Analyste monAnalyste = new Analyste(nom, prenom, date, code, annee);
        
        salarie.put(code, monAnalyste);
		
	}


	private static void listeConcepteur() {
		// TODO Auto-generated method stub
		
	}
	
	public static void listeSalarie(Map<Integer, Salarie> salarie2) {
        for (Integer i : salarie2.keySet()) {
            System.out.println("key: " + i + " value: " + salarie2.get(i).getNom());
        }
    }
	



public static void listeConcepteur(HashMap<Integer, Salarie> liste_salarie) {
    System.out.println("Voici la liste des concepteur :");
    for (Integer i : liste_salarie.keySet()) {
        if (liste_salarie.get(i) instanceof Concepteur) {
            System.out.println("id salarié: " + i + " Dénomination: " + liste_salarie.get(i).getNom()+" "+liste_salarie.get(i).getPrenom());
        }
    }
}

}


//0002-PESTANA-Cyril-23/12/2014-2






